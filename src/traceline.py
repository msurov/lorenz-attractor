import numpy
from numpy import array, vstack, append, zeros, repeat
import scipy
from scipy.integrate import ode


class TraceLine:
    def shift_one_step(self):
        x = self.x
        t = self.t
        solver = self.solver
        step = self.step

        solver.integrate(t[-1] + step)
        x = vstack((x, solver.y))
        t = append(t, solver.t)
        self.t = t[1:]
        self.x = x[1:]

    def update_for(self, t):
        n = int((t - self.t[-1]) / self.step)
        for i in range(0, n):
            self.shift_one_step()
        return n > 0

    def points(self):
        return self.t, self.x

    def __init__(self, rhs, x0, step, trace_len, t0=0.0):
        self.x0 = x0
        self.t0 = t0
        self.step = step
        self.trace_len = trace_len

        # init integrator
        solver = ode(rhs)
        solver.set_integrator('dopri5', atol=1e-2, nsteps=20, first_step=self.step/10.)
        solver.set_initial_value(self.x0, self.t0)
        self.solver = solver

        # integrate
        self.x = repeat([self.x0], trace_len, axis=0)
        self.t = repeat([self.t0], trace_len)


class TraceLineArr:
    def __init__(self, rhs, x0, step, trace_len, n_traces, t0=0.0):
        traces = []

        for i in xrange(n_traces):
            d = array([0, 0, -step * n_traces / 4. + step * i / 2.])
            trace = TraceLine(rhs, x0 + d, step, trace_len, t0)
            traces.append(trace)

        self.traces = traces
        self.trace_len = trace_len
        self.n_traces = n_traces
        self.n_particles = n_traces * trace_len

    def update_for(self, t):
        return any([trace.update_for(t) for trace in self.traces])

    def all_points(self):
        trace_len = self.trace_len
        pts = zeros((self.n_particles, 3))

        for i,trace in enumerate(self.traces):
            _,x = trace.points()
            n0 = i * trace_len
            n1 = (i + 1) * trace_len
            pts[n0:n1,:] = x

        return pts

    def __getitem__(self, idx):
        return self.traces[idx]
