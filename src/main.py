import time
import numpy as np
from vispy import gloo
from vispy import app
from traceline import TraceLineArr
from vispy.util.transforms import perspective, translate, rotate


VERT_SHADER = '''
#version 120

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;

attribute vec3 a_position;
attribute vec4 a_color;
attribute float a_size;

varying vec4 v_color;

void main (void)
{
    vec4 ccs_coords = u_view * u_model * vec4(a_position, 1.0);
    gl_Position = u_projection * ccs_coords;
    v_color = a_color;
    gl_PointSize = a_size * -100.0 / ccs_coords.z;
}
'''


FRAG_SHADER = '''
#version 120

varying vec4 v_color;

void main()
{
    float x = 2.0*gl_PointCoord.x - 1.0;
    float y = 2.0*gl_PointCoord.y - 1.0;
    float a = 1.0 - (x*x + y*y);
    gl_FragColor = vec4(v_color.rgb, a*v_color.a);
}
'''


class Canvas(app.Canvas):
    def get_time(self):
        if not hasattr(self, 't0'):
            self.t0 = time.time()
        return time.time() - self.t0

    def init_buffers(self):
        self.pt_radius = 8.

        n_particles = self.traces.n_particles
        trace_len = self.traces.trace_len
        n_traces = self.traces.n_traces

        pts = self.traces.all_points()
        self.buf_positions = pts.astype('float32')

        self.buf_colors = np.zeros((n_particles, 4), dtype='float32')
        self.buf_sizes = np.zeros((n_particles, 1), dtype='float32')

        for i in range(n_traces):
            color = np.random.rand(4)

            for j in range(trace_len):
                n = i * trace_len + j
                self.buf_colors[n,:] = color
                self.buf_colors[n,3] = 1.0 * j / trace_len
                self.buf_sizes[n] = self.pt_radius # * j / trace_len

    def on_resize(self, event):
        width, height = event.physical_size
        gloo.set_viewport(0, 0, width, height)
        self.apply_size()

    def on_draw(self, event):
        gloo.clear()
        self.program.draw('points')
        self.iteration()

    def iteration(self):
        changed = self.traces.update_for(self.get_time())
        if changed:
            pts = self.traces.all_points()
            # TODO: remove self.buf_positions
            self.buf_positions = pts.astype('float32')
            self.vbo_position.set_data(self.buf_positions.copy())

        self.cam_pos = translate([0, 0, self.translate]).dot(rotate(5. * self.get_time(), [0, 1, 0]))
        self.view = np.linalg.inv(self.cam_pos)
        self.program['u_view'] = self.view

    def apply_size(self):
        self.projection = perspective(45.0, self.size[0] / float(self.size[1]), 1.0, 300.0)
        self.program['u_projection'] = self.projection

    def __init__(self):
        app.Canvas.__init__(self, keys='interactive', title='Lorenz Attractor', fullscreen=True)

        t0 = self.get_time()
        x0 = np.random.rand(3) * 50.0
        n_traces = 20
        trace_len = 1000
        step = 0.004

        sigma,rho,beta = 10., 28., 8./3.
        rhs = lambda t, u: np.array([sigma*(u[1] - u[0]), u[0] * (rho - u[2]) - u[1], u[0] * u[1] - beta * u[2]]) / 4.
        self.traces = TraceLineArr(rhs, x0, step, trace_len, n_traces, t0)
        self.init_buffers()

        # viewport
        width, height = self.physical_size
        gloo.set_viewport(0, 0, width, height)

        # Create program
        self.program = gloo.Program(VERT_SHADER, FRAG_SHADER)

        # model coordinates -> world coordinates
        self.model = np.eye(4, dtype=np.float32)
        self.program['u_model'] = self.model
        # world coordinates -> camera coordinates
        self.translate = 100.
        self.cam_pos = translate([0, 0, self.translate]).dot(rotate(10, [0, 1, 0]))
        self.view = np.linalg.inv(self.cam_pos)
        self.program['u_view'] = self.view
        # camera coordinates -> 2D coordinates
        self.apply_size()

        # Create vertex buffers
        self.vbo_position = gloo.VertexBuffer(self.buf_positions.copy())
        self.vbo_color = gloo.VertexBuffer(self.buf_colors.copy())
        self.vbo_size = gloo.VertexBuffer(self.buf_sizes.copy())

        # Bind vertex buffers
        self.program['a_color'] = self.vbo_color
        self.program['a_size'] = self.vbo_size
        self.program['a_position'] = self.vbo_position

        gloo.set_state(
            clear_color=(0, 0, 0, 1),
            blend=True,
            blend_func=('src_alpha', 'one')
        )
        self._timer = app.Timer('auto', connect=self.update, start=True)
        self.show()


def run():
    c = Canvas()
    app.run()

if __name__ == '__main__':
    run()
